 (defproject job-queue "0.1.0-SNAPSHOT"
   :description "A task manager that assigns a given job to an agent with certain skills"
   :dependencies [[org.clojure/clojure "1.8.0"]
                  [metosin/compojure-api "1.1.11"]
                  [cheshire "5.8.0"]
                  [com.novemberain/monger "3.1.0"]]
   :ring {:handler job-queue.handler/app}
   :uberjar-name "server.jar"
   :profiles {:dev {:dependencies [[javax.servlet/javax.servlet-api "3.1.0"]
                                  [ring/ring-mock "0.3.0"]
                                  [midje "1.8.3"]]
                   :plugins [[lein-ring "0.12.0"]
                             [lein-midje "3.2"]]}})
