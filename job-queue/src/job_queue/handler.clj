(ns job-queue.handler
    (:require [compojure.api.sweet :refer :all]
      [job-queue.db :refer :all]
      [ring.util.http-response :refer :all]
      [ring.swagger.json-schema :as json-schema] ;; adding swagger-support for ObjectId
      [schema.core :as s]
      [cheshire.core :as cheshire]
      monger.json)
    (:import  [org.bson.types ObjectId]))



(defmethod json-schema/json-type ObjectId [_] {:type "string"})

(s/defschema newAgents {:_id org.bson.types.ObjectId
                        :agent_name s/Str
                        :primary_skillset [s/Str]
                        :secondary_skillset [s/Str]})

(s/defschema newJobs {:new_job {:_id org.bson.types.ObjectId
                                :type s/Str
                                :urgent s/Bool}})

(s/defschema newAssignedJob {:job_assigned {:job_id org.bson.types.ObjectId
                                            :agent_id org.bson.types.ObjectId}})

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Job-queue"
                    :description "A simple task manager"}
             :tags [{:name "api", :description "task manager"}]}}}

    (context "/api" []
      :tags ["api"]

      (GET "/agents" []
        :return [newAgents]
        :summary "gets the registered agents"
        (ok (getAllAgents) ))

      (POST "/agents" []
        :return newAgents
        :body [newagent newAgents]
        :summary "posts a new agent"
        (ok postAgent))

      (GET "/jobs" []
        :return newJobs
        :summary "gets the registered jobs"
        (ok getAllJobs))

      (POST "/jobs" []
        :return newJobs
        :body [job newJobs]
        :summary "posts a new job"
        (ok job))

      (GET "/assigned-jobs/:id" []
        :path-params [id :- String]
        :return newAssignedJob
        :summary "gets the registered assigned jobs"
        (ok getNewAssignedJob)))))
