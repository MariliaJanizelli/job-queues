# Task-Manager
A simple task manager written in clojure, using swagger.
This api uses Leiningen 2.8.1 on Java 1.8.0_141.
and MongoDB v3.4.7, it's necessary to have it installed.

## Usage

### Run the application locally

`lein ring server`

This will direct to the swagger interface on your localhost. It'll be possible to see the operations used. You may select the desired one and try it out!

### Run the tests

`lein midje`

### Observations

The operations were not completed. The test also had errors that could not be figured out. 
There was an attempt to use the library rageDB to construct a a in memory database, however unsuccessfully. Hence, a trial of using the native clojure feature, atom, was made, however, due to lack of understanding of such methods, those operations were not completed as desired.
The first test did not return the desired result, the output could not be fixed whatsoever and the lack of material was somewhat an obstacle, despite the slack community shared thei material to help to see the construction of clojure webservices. 
As this is a test, I found abusive to ask them for further help, and also for the hiring company members, as it feels like cheating and beyond the examination's purpose for the hiring process.
Given the difficulty, and some other minor factors that do not fit in this file,it's left here all that I could have developed.
It's necessary to point out that during a period of time, bitbucket was off, hence I worked safely on github, did all the work, moved it to my private repo on bitbucket and then erased the public work. 
Some of the steps will not be shown, as well it may be a mess to follow, as I created few helper projects aside, and back and forth adding to the main project, but this is the final work.

UPDATE:
It was added a database connection to store the inputs. As mongo uses ObjectId to store their ids, it's necessary to make a coercion process for it
to be read by clojure.
Adding information manually resulted in positive tests, that is, inserting documents is possible to accomplish the crud operations realatively simply,
However the insertion via parameters presented a few errors I could  not figure out, for the GET operation couldn't retrieve the _id;



