(ns job-queue.db
    (:require [monger.core :as mg]
      [monger.collection :as mc]
      monger.json
      monger.joda-time)
    (:import org.bson.types.ObjectId))

(let [conn (mg/connect )
      db (mg/get-db conn "monger-test")]

     (mc/insert db "agents" {:_id (ObjectId.) :agent_name "john doe" :primary_skillset ["some stuff"] :secondary_skillset []})
     (mc/insert db "jobs" {:_id (ObjectId.) :type ["bills"] :urgent false})

     (defn getAllJobs []
           (mc/find-maps db "jobs"))

     (defn getAllAgents []
           (mc/find-maps db "agents"))

     (defn postAgent [newagent]
           (mc/insert db "agents" newagent))

     (defn getAgentById [agent_id]
           (mc/find-one db "agents" {:_id (ObjectId. agent_id)}))

     (defn getJobsByType [type]
           (mc/find-as-map db "jobs" {:type type }))

     (defn getNewAssignedJob [agent_id]
           (defn firstSkill
                 [agent_id]
                 (get (first (getAgentById [agent_id])) :primary_skillset))
           (defn secondSkill
                 [agent_id]
                 (get (first (getAgentById [agent_id])) :secondary_skillset))
           (cond
             (empty? (firstSkill)) (getJobsByType [(firstSkill)])
                   :else (getJobsByType [(secondSkill)]))))


