(ns job-queue.handler-test
    (:require [cheshire.core :refer :all]
      [midje.sweet :refer :all]
      [job-queue.handler :refer :all]
      [ring.mock.request :as mock]))

(defn parse-body [body]
   (parse-string (slurp body) true))

(fact "Test POST request to /agents returns expected response"
      (let [new_agent {:new_agent {:_id           "5a60a0afcc71615ab0dd8207"
                                   :name               "John"
                                   :primary_skillset   ["bills"]
                                   :secondary_skillset []}
                       }
            response (app (-> (mock/request :post "/api/agents")
                              (mock/content-type "application/json")
                              (mock/body (generate-string new_agent ))))
            body (parse-body (:body response))]
           (:status response) => 200
           (println response)
           (:result body) => new_agent (println body))
      )


;(fact "Test GET request to /agents returns expected response"
;  (let [new_agent {:agent_id "001"
;                   :name "Bojack Horseman"
;                   :primary_skillset ["bills-questions"]
;                   :secondary_skillset []}
;        response (app (-> (mock/request :get "/api/agents")))
;        body (parse-body (:body response))]
;    (:status response) => 200
;    (:result body) => new_agent))
;
(fact "Test POST request to /jobs returns expected response"
    (let [new_job {:job_id "002"
                   :type "Bills questions"
                 :urgent true}
        response (app (-> (mock/request :post "/api/jobs")
                          (mock/content-type "application/json")
                          (mock/body  (cheshire/generate-string new_job))))
        body (parse-body (:body response))]
    (:status response) => 200
    (:result body) => new_job))
;
;(fact "Test GET request to /jobs returns expected response"
;  (let [new_job {:job_id "002"
;                 :type "Bills questions"
;                 :urgent true}
;        response (app (-> (mock/request :get "/api/jobs")))
;        body (parse-body (:body response))]
;    (:status response) => 200
;    (:result body) => new_job))
;
;(fact "Test GET request to /assigned-jobs/:agent_id returns expected response"
;  (let [job_assigned {:job_id "002"
;                      :agent_id "001"}
;        response (app (-> (mock/request :get "/api/assigned-job/001")))
;        body (parse-body (:body response))]
;    (:status response) => 200
;    (:result body) => job_assigned))
